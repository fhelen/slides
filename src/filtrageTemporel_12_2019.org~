#+DESCRIPTION: 
#+KEYWORDS: 
#+LANGUAGE:  fr
#+OPTIONS: H:1 ^:{} num:t toc:nil \n:nil @:t ::t |:t ^:t -:t f:t *:t <:t
#+OPTIONS:   TeX:t LaTeX:t skip:nil d:nil todo:t pri:nil tags:not-in-toc
#+INFOJS_OPT: view:nil toc:nil ltoc:nil mouse:underline buttons:0 path:http://orgmode.org/org-info.js
#+EXPORT_SELECT_TAGS: export
#+EXPORT_EXCLUDE_TAGS: noexport

#+startup: oddeven

#+startup: beamer
#+LaTeX_CLASS: beamer
#+LaTeX_CLASS_OPTIONS: [10pt]
#+LaTeX_CLASS_OPTIONS: [1pt,oneside,reqno]
#+latex_header: \inputencoding{utf8} 
#+latex_header: \usepackage[T1]{fontenc}
#+latex_header: \usepackage[french]{babel}
#+latex_header: \AtBeginSection[]{\begin{frame}<beamer>\frametitle{Sommaire}\tableofcontents[currentsection]\end{frame}}
#+latex_header: \useoutertheme{infolines} 
#+latex_header: \mode<beamer>{\usetheme{Pittsburgh}}
#+latex_header: \setbeamertemplate{navigation symbols}{} 
#+latex_header: \setbeamerfont{structure}{series=\bfseries}
#+latex_header: \setbeamertemplate{items}[triangle]
#+latex_header: \setbeamercolor{block title}{fg=blue!40!black}
#+latex_header: \newcommand{\shorttitle}{florian.helen@airbus.com}
#+latex_header: \newcommand{\shortauthor}{Florian.Hélen}
#+latex_header: \setbeamertemplate{footline}{\leavevmode\hbox{\begin{beamercolorbox}[wd=.4\paperwidth,ht=2.25ex,dp=1ex,left]{author in head/foot}  \usebeamerfont{author in head/foot}~~\shortauthor   \end{beamercolorbox}   \begin{beamercolorbox}[wd=.2\paperwidth,ht=2.25ex,dp=1ex,center]{title   in head/foot}     \usebeamerfont{title in head/foot}\shorttitle   \end{beamercolorbox}   \begin{beamercolorbox}[wd=.38\paperwidth,ht=2.25ex,dp=1ex,right]{date in head/foot}\usebeamerfont{date in head/foot}\insertshortdate{}\hspace*{1em}\insertframenumber{} / \inserttotalframenumber\hspace*{1ex} \end{beamercolorbox}}\vskip0pt}
#+latex_header: \usepackage{amsfonts,bm,amsmath,amssymb,ifsym,marvosym,tabularx,array,ifsym}
#+latex_header: \usepackage{tikz}
#+latex_header: \newcommand{\vns}{Ven$\mu$s}
#+latex_header: \newcommand\boxPlot[6] {  \pgfmathsetmacro\rectSize{0.3};  \draw[thick] (#2,#1) -- (#3,#1);  \draw[thick] (#2,#1-\rectSize/2) -- (#2,#1+\rectSize/2);  \draw[thick] (#5,#1) -- (#6,#1);  \draw[thick] (#6,#1-\rectSize/2) -- (#6,#1+\rectSize/2);  \draw[fill=white] (#3,#1-\rectSize) rectangle (#5,#1+\rectSize);  \draw (#4,#1-\rectSize) -- (#4,#1+\rectSize);}
#+latex_header: \def\G{\ensuremath{{\cal G}}}
#+latex_header: \newcommand{\putat}[3]{\begin{picture}(0,0)(0,0)\put(#1,#2){#3}\end{picture}}
#+latex_header: \usetikzlibrary{arrows,fit,backgrounds,positioning,shapes,shadows}



#+BEAMER_FRAME_LEVEL: 1

#+COLUMNS: %80ITEM %13BEAMER_env(Env) %6BEAMER_envargs(Args) %4BEAMER_col(Col) %7BEAMER_extra(Extra)

#+TITLE: Séries temporelles: filtrage temporel 
#+AUTHOR: Florian Helen
#+EMAIL: florian.helen@airbus.com
* Plan de la présentation
+ Problématique
+ Méthode
  + Modèles phénologiques
  + Inversion
  + Filtrage des résidus
  + Identifications des outliers   
+ API 
  + Endpoints
  + Environement GCP
  + Exemple
* Problématique
- Des séries temporelles bruitées 
- Des séries temporelles irrégulièrement distribuées (temps & espace)
- Intérêt pour une méthode pour:
    - diminuer le bruits observé;
    - identifier les points aberrents;  
    - décrire le cycle phénologique des cultures observées;
    - fournir une information témporelle régulière à un pas de temps régulier (décision de l'utilisateur); 

#+begin_src gnuplot  :var data="../data/profile.dat" :file "../figures/profile.png"  :results outputs :exports none
reset
set key autotitle columnhead
set grid
set ytics font ",10"
set xtics font ",10" rotate 90
set term png
set key box rigth
set xlabel "Doy" font ",10"
set ylabel "LAI*1000" font ",10"
plot '$data' u 1:2:xtic(1)  t "Observations" w p ps 3;
#+end_src

#+ATTR_LATEX: :width 0.5\textwidth 
[[file:../figures/profile.png]]


* Modèles phénologiques
** Col left :BMCOL:
   :PROPERTIES:
   :BEAMER_col: 0.6
   :END:
+ 4 modèles phénologiques:
  - céréales d'hivers;
  - colza;
  - cultures d'été (maïs & soja);
  - autres cultures (pomme de terres, céréales de printemps,...);

+ Bâtis autour d'un modèle commun:
\begin{equation} 
G(x,t) = A \times (\frac{1}{1+e^{\frac{4}{x_1} \times (x_{0}-t)}}-\frac{1}{1+e^{\frac{4}{x_3} \times (x_{2}-t)}})
\end{equation}

** Col right                                                          :BMCOL:
   :PROPERTIES:
   :BEAMER_col: 0.4
   :END:

#+begin_src gnuplot :var x0=75 :var x1=7 :var x2=250 :var x3=10 :var A=5 :var B=0.1 :file "../figures/doublelog.png" :results outputs :exports none
reset
set grid on
set xrange [1:365]
set yrange [0:A+0.5]
set xlabel "DoY"
set ylabel "LAI"
set label "phase de croissance"  at 30,5.3
set label "phase de scenescence" at 170,5.3
set arrow from 0,5.2 to 160,5.2 heads lw 2 lc rgb 'green'
set arrow from 160,5.2 to 300,5.2 heads lw 2 lc rgb 'blue'
set arrow from 160,graph 0 to 160,graph 2 nohead
plot A*(1.0/(1.0+exp((x0-x)/x1))-1.0/(1.0+exp((x2-x)/x3)))  t '';
#+end_src

[[file:../figures/doublelog.png]]


* Modèles phénologiques
+ Des paramètres qui ont un sens agronomique:
  - $A$    : maximum admis pour un paramètre biophysique & pour un type de culture;
  - $x_{0}$: intensité maximum de croissance (jour de l'année);
  - $x_{1}$: durée de la phase de croissance  (nb de jours);
  - $x_{2}$: instensité maximum de scénéscence (jour de l'année);
  - $x_{3}$: dureé de la phase de scénéscence (nb jours);
  - paramètres spécifiques aux modèles (intensité de croissance hivernale, fraction de perte, intensité de maturité); 
#+begin_src python :var l="../figures/models.png" :file="../figures/models.png" :results outputs :exports none
  import numpy as np
  from cropLab import PhenoModels,genDicoCropParams
  import matplotlib.pylab as plt
  p_sc = genDicoCropParams([5.,180.,30,250,10,0.01],"CORN")
  p_wc = genDicoCropParams([-60,1.2,5.,110.,30.,165.,10.],"WHEAT")
  p_rc = genDicoCropParams([0.025,-60,0.8,30,50,5.,110.,30.,165.,10.],"RAPESEED")
  p_oc = genDicoCropParams([7.,200.,30,250,10],"OTHERCROPS")
  doys = np.arange(-100,300)
  P_sc = PhenoModels(p_sc)
  P_wc = PhenoModels(p_wc)
  P_rc = PhenoModels(p_rc)
  P_oc = PhenoModels(p_oc)
  m_sc = P_sc.wrapperModel(doys,"CORN")
  m_wc = P_wc.wrapperModel(doys,"WHEAT")
  m_rc = P_rc.wrapperModel(doys,"RAPESEED")
  m_oc = P_oc.wrapperModel(doys,"OTHERCROPS")
  fig,(ax1,ax2,ax3,ax4) = plt.subplots(4,sharex = True)
  ax1.plot(doys,m_sc,'k--',label = "CORN,SOYBEAN")
  ax1.grid()
  ax1.legend()
  ax2.plot(doys,m_wc,'k--',label = "WHEAT,BARLEY")
  ax2.grid()
  ax2.legend()
  ax3.plot(doys,m_rc,'k--',label = "RAPESEED")
  ax3.grid()
  ax3.legend()
  ax4.plot(doys,m_oc,'k--',label = "Othercrops")
  ax4.grid()
  ax4.legend()
  plt.savefig(f)
  return f
#+end_src

#+RESULTS:

#+ATTR_LATEX: :width 0.4\textwidth 
[[/stock/presentations/figures/models.png]]

* Inversion
Objectif: obtenir la meilleure estimation des paramètres du modèle ($\widehat x_{j}$) compte tenu des observations;
+ Shéma d'inversion: 
\begin{equation}
\min_{x} C(x,\bar{x})
\end{equation}

+ Fonction de coût:
\begin{equation}
C(x,\bar{x}) = \sqrt{\frac{1}{N_{i}}\times\sum_{i}{\frac{1}{\sigma_{i}^{2}}(y_{i}(x)-\widehat{y}_{i}(x)})^{2}} + \sqrt{\frac{1}{N_{j}}\times\sum_{j}\frac{1}{\sigma_{j}^{2}}(x_{j}-\bar{x_j})^{2}}
\end{equation}

+ Optimisation: Levenberg-Marquart cite:ranganathan2004levenberg
* Filtrage des résidus
Objectif: filtrer les observations en dehors du cycle cultural;
- Prise en compte des intercultures & des repousses;
+ Algorithme: Whittaker smoother cite:eilers2003perfect
    - smoother & interpoler 
    - fenêtre temporelle 
    - ordre de pénalité 
* Identification des outliers 
+ Objectifs:
  - identifier les observations nuageuses (faibles valeurs);
  - identifier les observations anormalement élevées;
+ Algorithme
  - Erreur asymptotique de l'ajustement $\sigma_{y}$;  
  - Interval de confiance à 95%;

#+ATTR_LATEX: :width 0.8\textwidth 
[[../figures/outliers.png]]

* API
+ Méthodes déployées sur l'environement /GCP/;
+ Projet Verde-dev;
  - Tester & qualifier les méthodes;
+ URL du service: /https://verde-filtrage-6ig3f4nobq-ew.a.run.app/ 
+ 3 endpoints:
  -  /model  : retourne les valeurs du modèle sur une plage temporelle;
  -  /filter : retourne les valeurs du filtre sur une plage temporelle;
  -  /outlier: retourne la liste des observations avec un flag (booleen);  
* Environement GCP
+ Technologies : /C++/ & /Go/
+ Bibliothèque : ITK cite:ITKSoftwareGuide
+ Dockerfile 
+ Cloud Run:
  - scalabilité automatique;
  - sans infrastructure (/serverless/);
* Exemple: filtrage  
+ Payload 
#+begin_example
{"observations": 
[{"id": "Id_0", 
"sensingDate": "2018-09-02T15:35:33.783Z", 
"sensor": "SENTINEL2A", 
"value": 0.1132, 
"layer": "LAI", 
"crop": "WHEAT"},..,],
"startDate": "2018-09-01", 
"endDate": "2019-09-01"}
#+end_example
* Exemple: filtrage  
#+begin_src python :results file :exports none :var fileName="../figures/example.png" :file='../figures/example.png' 
import json,requests
from datetime import datetime
from cropLab import formatJson
from DAO import Dao
import matplotlib.pyplot as plt
dbFile = '/home/fhelen/workspace/database/passDb.txt'
D = Dao(dbFile,True)
listId = D.getScId("LAI","Winter wheat")
ts = D.getScTimeSeries("LAI",listId[10][0])
j = formatJson(ts,"LAI","2018-09-01","2019-09-01")
r = requests.post('https://verde-filtrage-6ig3f4nobq-ew.a.run.app/filter',data=j)
f  = [float(element['Value']) for element in r.json()['Filter']]
dd = [datetime.strptime(element['Date'],"%Y-%m-%d") for element in r.json()['Filter']]
plt.plot(dd,f,'k--',label = "Filter")
plt.scatter(ts[0]["acquisitions"],ts[0]['observations'],marker="o",color='r',label="observations")
plt.legend()
plt.grid()
plt.savefig(fileName)
return fileName
#+end_src

#+begin_example
import json
import requests as r
r.post('https://verde-filtrage-6ig3f4nobq-ew.a.run.app/filter',
       data=j)
#+end_example

#+ATTR_LATEX: :width 0.6\textwidth 
[[file:../figures/example.png]]

  
* References
 :PROPERTIES:
 :BEAMER_opt: allowframebreaks,label=
 :END:
 bibliographystyle:unsrt
 bibliography:/stock/biblio/bib/ref.bib





